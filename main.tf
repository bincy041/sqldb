terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 2.43.0"
    }
  }
}
provider "azurerm" {
  features {
  }
}


resource "azurerm_resource_group" "test" {
  name     = "ResourceGroup_sql"
  location = "West US"
}

resource "azurerm_sql_server" "test" {
  name                         = "binsqldbserver"
  resource_group_name          = "${azurerm_resource_group.test.name}"
  location                     = "West US"
  version                      = "12.0"
  administrator_login          = "azureuser1"
  administrator_login_password = "Qwer1234!@#$"
}

resource "azurerm_sql_database" "test" {
  name                = "mysqldatabase"
  resource_group_name = "${azurerm_resource_group.test.name}"
  location            = "West US"
  server_name         = "${azurerm_sql_server.test.name}"

  tags = {
    environment = "production"
  }
}